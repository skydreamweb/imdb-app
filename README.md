# project: skydream_imdb

## Project setup

```
npm install
```

### For using http://www.omdbapi.com/ API for API_KEY is needed

```
create .env file on root which includes
VUE_APP_API_KEY=
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```
