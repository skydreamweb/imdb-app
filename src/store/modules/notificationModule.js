export const state = {
  loading: false,
  error: ''
}

export const mutations = {
  CHANGE_LOADING: (state, isLoading) => {
    state.loading = isLoading
  },
  SET_ERROR: (state, error) => {
    state.error = error
  },
  DELETE_ERROR: state => {
    state.error = ''
  }
}

export const actions = {
  setLoading: ({ commit }, isLoading) => {
    commit('CHANGE_LOADING', isLoading)
  },
  error: ({ commit }, error) => {
    commit('SET_ERROR', error)
  },
  removeError: ({ commit }) => {
    commit('DELETE_ERROR')
  }
}

export const getters = {
  loading: state => state.loading,
  error: state => state.error
}
