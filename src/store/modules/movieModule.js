import apiService from '@/services/apiService'
import NProgress from 'nprogress'

// MOVIES STATE
export const state = {
  movies: [],
  movie: {},
  favorites: [],
  title: '',
  year: '',
  type: '',
  page: 1,
  count: 0
}

// MOVIES MUTATIONS
export const mutations = {
  SET_SEARCH_INFO: (state, { title, year, type, page }) => {
    state.title = title
    state.year = year
    state.type = type
    state.page = page
  },
  SET_COUNT: (state, count) => {
    state.count = count
  },
  SET_MOVIES: (state, movies) => {
    state.movies = movies
  },
  SET_MOVIE: (state, movie) => {
    state.movie = movie
  },
  ADD_FAVORITE: (state, movie) => {
    state.favorites.push(movie)
  },
  REMOVE_FAVORITE: (state, id) => {
    state.favorites = state.favorites.filter(movie => movie.imdbID !== id)
  }
}

// MOVIES ACTIONS
export const actions = {
  fetchMovies: (context, info) => {
    context.dispatch('setLoading', true)
    NProgress.start()
    const { title, year, type, page } = info
    context.commit('SET_SEARCH_INFO', { title, year, type, page })
    return apiService
      .getMovies(title, year, type, page)
      .then(res => {
        if (res.data.Response === 'True') {
          // remove error
          context.dispatch('error', '')
          context.commit('SET_COUNT', res.data.totalResults)
          context.commit('SET_MOVIES', res.data.Search)
          // finish loading
          context.dispatch('setLoading', false)
          NProgress.done()
        } else {
          // set error
          context.dispatch('error', res.data.Error)
          // finish loading
          context.dispatch('setLoading', false)
          NProgress.done()
        }
      })
      .catch(err => {
        // set error
        console.log(err.response)
        context.dispatch('error', err.response.message)
        // finish loading
        context.dispatch('setLoading', false)
        NProgress.done()
      })
  },

  editFavorites: (context, id) => {
    // find movie in state.movies using getter movies
    const movie = context.getters.movies.find(movie => movie.imdbID === id)
    // if movie is already in favorites - remove it
    if (context.getters.isFavorite(id)) {
      context.commit('REMOVE_FAVORITE', id)
    } else if (movie) {
      // if movie is not in favorites but is already fetched - add to favorites
      context.commit('ADD_FAVORITE', movie)
    } else {
      // if movie not found in favorites or fetched movies - fetch from api and add to favorites
      // start loading
      context.dispatch('setLoading', true)
      NProgress.start()

      apiService
        .getMovieById(id)
        .then(res => {
          if (res.data.Response === 'True') {
            // remove error
            context.dispatch('error', '')
            context.commit('ADD_FAVORITE', res.data)
            // finish loading
            context.dispatch('setLoading', false)
            NProgress.done()
          } else {
            // add error
            context.dispatch('error', res.data.Error)
            // finish loading
            context.dispatch('setLoading', false)
            NProgress.done()
          }
        })
        .catch(err => {
          console.log(err.response)
          // add error
          context.dispatch('error', err.response.message)
          // finish loading
          context.dispatch('setLoading', false)
          NProgress.done()
        })
    }
  },

  // fetch movie by ID with all details
  fetchMovieById: (context, id) => {
    // start loading
    NProgress.start()
    context.dispatch('setLoading', true)
    return apiService
      .getMovieById(id)
      .then(res => {
        if (res.data.Response === 'True') {
          // remove error
          context.dispatch('error', '')
          context.commit('SET_MOVIE', res.data)
          // finish loading
          context.dispatch('setLoading', false)
          NProgress.done()
          // return movie for using it in router/index.js
          return res.data
        } else {
          // add error
          context.dispatch('error', res.data.Error)
          // finish loading
          context.dispatch('setLoading', false)
          NProgress.done()
        }
      })
      .catch(err => {
        console.log(err.response)
        // add error
        context.dispatch('error', err.response)
        // finish loading
        context.dispatch('setLoading', false)
        NProgress.done()
      })
  }
}

// MOVIES GETTERS
export const getters = {
  movies: state => {
    return state.movies
  },
  favorites: state => {
    return state.favorites
  },
  year: state => {
    return state.year
  },
  title: state => {
    return state.title
  },
  type: state => {
    return state.type
  },
  count: state => {
    return state.count
  },
  page: state => {
    return state.page
  },
  // check by imdbID (id) if movie is in favorites
  isFavorite: state => id => {
    return state.favorites.find(movie => movie.imdbID === id) ? true : false
  },
  // check by imdbID (id) if movie is in fetched movies
  movie: state => id => {
    return state.movies.find(movie => movie.imdbID == id)
  }
}
