import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Favorites from '../views/Favorites.vue'
import Details from '../views/Details.vue'
import store from '@/store'
import NProgress from 'nprogress'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/favorites',
    name: 'Favorites',
    component: Favorites
  },
  {
    path: '/details/:id',
    name: 'Details',
    component: Details,
    props: true,
    beforeEnter(to, from, next) {
      store.dispatch('fetchMovieById', to.params.id).then(movie => {
        to.params.movie = movie
        next()
      })
    }
  },
  // for every other route navigate to home
  {
    path: '*',
    redirect: '/'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// loading
router.beforeEach((to, from, next) => {
  NProgress.start()
  next()
})

router.afterEach((to, from) => {
  NProgress.done()
})

export default router
