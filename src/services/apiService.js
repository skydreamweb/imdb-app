import axios from 'axios'

// API KEY is stored in .env file
const apiClient = axios.create({
  baseURL: `https://www.omdbapi.com/`,
  withCredentials: false,
  header: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  params: {
    apikey: process.env.VUE_APP_API_KEY
  }
})

export default {
  // search by title, s is mandatory
  getMovies(title, year, type, page) {
    return apiClient.get('/', {
      params: { s: title, type, y: year, page }
    })
  },
  // get movie by imdbID
  getMovieById(id) {
    return apiClient.get('/', {
      params: { i: id }
    })
  }
}
